﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2parcial
{
    class Program
    {

        static void Main(string[] args)
        {
            int[] Dinero = new int[] { 5, 10, 25, 50, 100, 200 };

            List<Productos> productos = new List<Productos>();
            int[] ID = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            string[] Nombres = { "Coca cola", "Agua", "Barra Milky Way", "Emanems M&M", "Oreo", "Doritos", "Ruffles Jamon", "Monster", "Cheetos Picantes", "Barra Twix" };
            int[] Precios = { 20, 10, 30, 15, 20, 15, 20, 120, 30, 25 };
            int[] Existencia = { 4, 2, 2, 3, 6, 7, 5, 8, 7, 5 };

            for (int i = 0; i < Nombres.Length; i++)
            {
                productos.Add(new Productos() { ID = ID[i], Nombre = Nombres[i], Precio = Precios[i], Existencia = Existencia[i] });
            }

            Console.WriteLine("\n-->       MAQUINA EXPENDEDORA        <--\n");

            Console.WriteLine();
            foreach (Productos Articulos in productos)
            {
                Console.WriteLine($"{Articulos.ID}  - Articulo: {Articulos.Nombre} -- Precio: {Articulos.Precio}");
            }
            Console.WriteLine("---------------------------------------");
            Console.Write("Introduce el dinero: ");

            try
            {
                int DineroIngresado = int.Parse(Console.ReadLine());
                int DineroTotal = 0;

                if (Dinero.Contains(DineroIngresado))
                {
                    DineroTotal = DineroIngresado;
                    Console.WriteLine("");
                    Console.Write("Eligir El Articulo: ");
                    int opcion = int.Parse(Console.ReadLine());
                    var data = productos.Where(x => x.ID == opcion).ToList();
                    Console.WriteLine();
                    foreach (var i in data)
                    {
                        Console.WriteLine($"{i.Nombre} -- {i.Precio}$ ");
                        Console.WriteLine("");
                        Console.WriteLine("-------------");
                        Console.WriteLine("1 - Confirmar");
                        Console.WriteLine("2 - Cancelar");
                        Console.WriteLine("-------------");
                        Console.Write("Elige una opcion: ");
                        int confirmar = int.Parse(Console.ReadLine());
                        if (confirmar == 1)
                        {
                            Console.WriteLine("");
                            int compra = (DineroTotal - i.Precio);
                            if (compra >= 0)
                            {
                                int DineroRestante = (DineroTotal - i.Precio);

                                Console.WriteLine($"Compra confirmada, dinero restante: {DineroRestante}");
                                Console.WriteLine($"Articulo elegido: {i.Nombre}");
                                i.ID = (i.ID - 1);
                                Console.WriteLine($"Existencia: {i.ID}");
                            }
                            else
                            {
                                Console.WriteLine("Dinero insuficiente");
                            }
                        }
                        else
                        {
                            Console.WriteLine($"Cancelando compra y regresando el dinero: {DineroTotal}$");
                        }
                    }

                }
                else
                {
                    Console.WriteLine("No se admite ese tipo de dinero, Favor introducir el dinero permitido");
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("valor invalido");
            }
            Console.ReadKey();
        }
        public class Productos
        {
            public int ID { get; set; }
            public string Nombre { get; set; }
            public int Precio { get; set; }
            public int Existencia { get; set; }
        }



        //static void Main(string[] args)
        //{




        //int SUMREFRE, totalg, op, V, h = 0, p = 0, s = 0, sl = 0;
        //string PA, NOM;
        //double a, b, c, d, e, suma = 0;
        //dispensadora dis = new dispensadora();
        //do
        //{
        //    Console.WriteLine("\n\t\t maquina de refrescos ~CHEMO~ ");
        //    Console.WriteLine("\n\t\t 1.- inserte las monedas");
        //    Console.WriteLine("\n\t\t 3.- comprar");
        //    Console.WriteLine("\n\t\t 4.- SALIR");
        //    Console.Write("\n\t\t opcion: ");
        //    V = Convert.ToInt16(Console.ReadLine());

        //    switch (V)
        //    {
        //        case 1:

        //            Console.Clear();
        //            Console.WriteLine("\n\t\t inserte la(s) moneda(s) de 50 centavos");
        //            dis.validarmoneda(s = Convert.ToInt32(Console.ReadLine()));
        //         Console.Clear();
        //            break;
        //    }

        //}
        //}
        //public class producto
        //{
        //    public string codigo { get; set; }
        //    public string nombre { get; set; }
        //    public double   precio { get; set; }
        //    public double cambio { get; set; }
        //    public int cantidad { get; set; }
        //    public double moneda { get; set; }
        //    public string existencia { get; set; }



        //    public void sumarCantidad(int cantidad)
        //    {
        //        this.cantidad += cantidad;
        //    }
        //    public bool validarCantidad()
        //    {
        //        if (this.cantidad > 0)
        //        {
        //            return true;
        //        }
        //        return false;
        //    }
        //    public bool validarvalor(double valor)
        //    {
        //        if(this.precio >= valor)
        //        {
        //            this.cambio = valor - this.precio;
        //            return true;
        //        }
        //        return false;
        //    }
        //    public void restarcantidad()
        //    {
        //        this.cantidad--;
        //    }
        //}

        //public class dispensadora
        //{
        //    public List<producto> producto { get; set; }
        //    public string pago { get; set; }

        //    public dispensadora()
        //    {
        //        producto arroz = new producto();
        //        arroz.codigo = "02";
        //        arroz.nombre = "la galza";
        //        arroz.precio = 300;
        //        arroz.existencia = " disponible";

        //        producto maiz = new producto();
        //        maiz.codigo = "03";
        //        maiz.nombre = "mazorca ";
        //        maiz.precio = 50;
        //        maiz .existencia = " disponible";

        //        producto platano = new producto();
        //        platano.codigo = "04";
        //        platano.nombre = "rulo";
        //        platano.precio = 30 ;
        //        platano.existencia = " disponible";

        //        producto vino = new producto();
        //        vino.codigo = "05";
        //        vino.nombre = "leon";
        //        vino.precio = 300;
        //        vino.existencia = " disponible";

        //        producto pasa = new producto();
        //        pasa.codigo = "05";
        //        pasa.nombre = "mi caquito";
        //        pasa.precio = 70;
        //        pasa.existencia = " disponible";


        //        producto salami = new producto();
        //        salami.codigo = "06";
        //        salami.nombre = "induveca";
        //        salami.precio = 150;
        //        salami.existencia = " disponible";

        //        producto cocacola = new producto();
        //        cocacola.codigo = "07";
        //        cocacola.nombre = "pezci cola";
        //        cocacola.precio = 65;
        //        cocacola.existencia = " disponible";

        //        producto papita = new producto();
        //        papita.codigo = "08";
        //        papita.nombre = "lays";
        //        papita.precio = 25;
        //        papita.existencia = " disponible";

        //        producto galleta = new producto();
        //        galleta.codigo = "09";
        //        galleta.nombre = "princesa";
        //        galleta.precio = 5;
        //        galleta.existencia = " disponible";

        //        producto habichuela = new producto();
        //        habichuela.codigo = "10";
        //        habichuela.nombre = "gira";
        //        habichuela.precio = 85;
        //        habichuela.existencia = " disponible";

        //        producto m = new producto();
        //        m.moneda = 5;
        //        m.moneda = 10;
        //        m.moneda = 25;
        //        m.moneda = 50;
        //        m.moneda = 100;
        //        m.moneda = 200;

        //    }
        //    public double validarmoneda(string[] moneda)
        //    {
        //        double total = 0;
        //        foreach (string item in moneda)
        //        {
        //            try
        //            {
        //                total += float.Parse(item);

        //            }
        //            catch (Exception e) { }
        //        }
        //        return total;

        //    }

        //    public int validarproducto(string codigo)
        //    {
        //        int encontro = -1;
        //        for (int i =0; i< this.producto.Count; i++)
        //        {
        //            if (this.producto[i].codigo==codigo)
        //                {
        //                encontro = 1;
        //                }
        //         }
        //            return encontro;
        //    }
        //    public bool agregarproducto(producto producto)
        //    {
        //        int enc = this.validarproducto(producto.codigo);
        //        if (enc >= 0)
        //        {
        //            this.producto[enc].sumarCantidad(producto.cantidad);
        //        }
        //        else
        //        {
        //            this.producto.Add(producto);
        //        }
        //        return true;
        //    }

        //    public producto vender (string codigo)
        //    {
        //        int enc = this.validarproducto(codigo);
        //        if (enc >=0)
        //        {
        //            if (this.producto[enc].validarCantidad())
        //            {
        //                string[] monedas = this.pago.Split('-');
        //                double total = this.validarmoneda(monedas);
        //                if (this.producto[enc].validarvalor(total))
        //                {
        //                    this.

        //                }
        //            }

        //        }
        //        return null;
        //    }
        //}

    }
}
